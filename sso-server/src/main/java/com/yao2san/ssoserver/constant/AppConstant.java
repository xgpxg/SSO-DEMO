package com.yao2san.ssoserver.constant;

/**
 * Created by wxg on 2019/5/5 13:49
 */
public class AppConstant {
    public static final String REDIS_TICKET_PREFIX = "TICKET:";

    public static final int REDIS_TICKET_ALIVE_SECONDS = 60;

    public static final boolean ENABLE_DISPOSABLE_TICKET = false;
}
