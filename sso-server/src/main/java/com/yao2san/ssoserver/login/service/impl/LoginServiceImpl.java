package com.yao2san.ssoserver.login.service.impl;

import com.yao2san.ssoserver.login.service.LoginService;
import com.yao2san.ssoserver.util.EncryptUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpSession;

/**
 * Created by wxg on 2019/5/4 19:35
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Override
    public String getTicket(String userName) {
        return null;
    }

    @Override
    public String createTicket(String uuid) {
        return  DigestUtils.md5DigestAsHex((EncryptUtil.SALT+uuid+System.currentTimeMillis()).getBytes());
    }

    @Override
    public boolean login(String userName, String password) {
        return userName.equalsIgnoreCase(password);
    }

    @Override
    public boolean logout(String userName) {
        return false;
    }
}
